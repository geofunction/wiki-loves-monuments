# Makefile per articolo in LaTeX

default: all

all:
	make clean
	latex wiki-loves-monuments.tex
	#makeindex -s index.ist thesis.idx
	latex wiki-loves-monuments.tex
	#latex wiki-loves-monuments.tex
	dvips -t landscape wiki-loves-monuments.dvi -o
	ps2pdf wiki-loves-monuments.ps
		
.tex:
	latex $@
	dvips -t a4 $?.dvi -o
	ps2pdf $@.ps
	
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.ps
	rm -f *.out
	rm -f *.nav
	rm -f *.snm
	rm -f *~
erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make theis: genera i file .dvi e .pdf " 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
